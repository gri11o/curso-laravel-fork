@extends('Template.main')

@if(count($tablaAmortizacion))
    @section('table-index')
        <table>
            <tr>
                <td>Cliente</td>
                <td>No. Pago</td>
                <td>Fecha</td>
                <td>Monto</td>
                <td>Intereses</td>
            </tr>
            @foreach($tablaAmortizacion as $pago)
                <tr>
                    <td>{{ $pago->cliente->name }}</td>
                    <td>{{ $pago->no_pago }}</td>
                    <td>{{ $pago->fecha }}</td>
                    <td>{{ $pago->monto }}</td>
                    <td>{{ $pago->interes }}</td>
                </tr>
            @endforeach
        </table>
        <hr>

        {{ $tablaAmortizacion->links() }}
    @endsection
@endif