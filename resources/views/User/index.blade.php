@extends('Template.main')

@if(count($users))
    @section('table-index')
        <table>
            <tr>
                <td>Nombre</td>
                <td>Email</td>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                </tr>
            @endforeach
        </table>
    @endsection
@endif