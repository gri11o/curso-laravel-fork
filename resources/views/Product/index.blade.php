@extends('Template.main')

@if(count($products))
    @section('table-index')
        <table>
            <tr>
                <td>Nombre</td>
                <td>Descripci&oacute;n</td>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->description }}</td>
                </tr>
            @endforeach
        </table>
        <hr>
        {{ $products->total() }} registros en total
        {{ $products->firstItem() }} de {{ $products->lastItem() }}

        {{ $products->links() }}
    @endsection
@endif