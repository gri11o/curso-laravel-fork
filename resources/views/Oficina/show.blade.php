@extends('Template.main')

@section('table-index')
    <table>
        <tr>
            <td>Nombre</td>
            <td>{{ $oficina->name }}</td>
        </tr>
        <tr>
            <td>Direcci&oacute;n</td>
            <td>{{ $oficina->address }}</td>
        </tr>
        <tr>
            <td>Tel&eacute;fono</td>
            <td>{{ $oficina->phone }}</td>
        </tr>
    </table>

    <a href="{{ route("oficina.index") }}">Regresar</a>
@endsection