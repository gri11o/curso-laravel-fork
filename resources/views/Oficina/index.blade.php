@extends('Template.main')

@if(count($oficinas))
    @section('table-index')
        {{-- @if(Auth::user()->hasRole('admin')) --}}
        {{-- @if(Entrust::hasRole('admin')) --}}
        @role('owner')
            <a href="{{ route('oficina.create') }}" class="btn btn-info">Nueva oficina</a>
        @endrole

        Usuario: {{ $user_name }}

        <table>
            <tr>
                <td>Nombre</td>
                <td>Domicilio</td>
                <td>Tel&eacute;fono</td>
            </tr>
            @foreach($oficinas as $oficina)
                <tr>
                    <td>{{ $oficina->name }}</td>
                    <td>{{ $oficina->address }}</td>
                    <td>{{ $oficina->phone }}</td>
                </tr>
            @endforeach
        </table>
    @endsection
@endif