<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tabla-amortizacion/{clienteId}', 'TablaAmortizacionController@index')->name('tabla-amortizacion')->middleware('auth');

Route::prefix('catalogos')->group(function () {
    Route::get('/product', 'ProductController@index')->name('product-list');

    Route::resource('oficina', 'OficinaController')->middleware('auth');
    Route::resource('user', 'UserController')->middleware('auth');
    //Route::resource('user', 'UserController')->except(['create', 'edit'])->middleware('auth');
    //Route::resource('user', 'UserController')->only(['destroy', 'create'])->middleware('auth');
});

// user.index => "Listado de registros"

// user.create => "Mostrar pantalla de nuevo registro"
// user.store => "Guardado de nuevo registro"

// user.edit => "Mostrar pantalla de registro existente"
// user.update => "Actualizar registro existente"

// user.destroy => "Eliminar registro existente"

// user.show => "Mostrar información de registro existente"
