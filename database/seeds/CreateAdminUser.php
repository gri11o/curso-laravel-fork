<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Permission;
use App\Role;
use App\User;

class CreateAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret123'),
        ]);

        $owner = new Role();
        $owner->name         = 'owner';
        $owner->display_name = 'Project Owner'; // optional
        $owner->description  = 'User is the owner of a given project'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();

        $user = User::where('email', '=', 'admin@gmail.com')->first();

        $user->attachRole($admin); // parameter can be an Role object, array, or id

        // or eloquent's original technique
        // $user->roles()->attach($admin->id); // id only

        $createOficina = new Permission();
        $createOficina->name         = 'create-oficina';
        $createOficina->display_name = 'Crear Oficina';
        $createOficina->description  = 'Crear nuevo registro de oficina'; // optional
        $createOficina->save();

        $editOficina = new Permission();
        $editOficina->name         = 'edit-oficina';
        $editOficina->display_name = 'Editar Oficina'; // optional
        $editOficina->description  = 'Editar registro de oficina'; // optional
        $editOficina->save();

        $admin->attachPermission($createOficina);
        $owner->attachPermissions(array($createOficina, $editOficina));
    }
}
