<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TablaAmortizacion extends Model
{
    protected $table = 'tabla_amortizacion';

    protected $fillable = [
        'no_pago', 'fecha', 'cliente_id', 'monto', 'interes'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\User', 'cliente_id');
    }
}
