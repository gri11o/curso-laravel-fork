<?php

namespace App\Http\Controllers;

use Auth;
use App\Oficina;
use Illuminate\Http\Request;

class OficinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oficinas = Oficina::all();

        return view('Oficina.index')->with(['oficinas' => $oficinas, 'title' => 'Listado de Oficinas del sistema', 'user_name' => session('user_name')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasRole('owner')) {
            return redirect()->route('oficina.index');
        }
        return view('Oficina.create')->with('title', 'Crear nueva oficina');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $oficinaNueva          = new Oficina;
        $oficinaNueva->name    = $request->input('name');
        $oficinaNueva->name    = $request->name;
        $oficinaNueva->address = $request->get('address');
        $oficinaNueva->phone   = $data['phone'];
        $oficinaNueva->save();

        $request->session()->flash('success', 'El registro de la oficina fue guardado exitosamente !');

        return redirect()->route('oficina.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oficina  $oficina
     * @return \Illuminate\Http\Response
     */
    public function show(Oficina $oficina)
    {
        return view('Oficina.show')->with(['oficina' => $oficina, 'title' => 'Registro: ' . $oficina->id . ' de oficinas']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oficina  $oficina
     * @return \Illuminate\Http\Response
     */
    public function edit(Oficina $oficina)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oficina  $oficina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oficina $oficina)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oficina  $oficina
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oficina $oficina)
    {
        //
    }
}
