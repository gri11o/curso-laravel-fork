<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Product;
use Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $isAuth = Auth::check();
        $currentUser = Auth::user();
        //$products = Product::all();
        //$products = DB::select('select * from products where id = :id', ['id' => 1]);

        $products = Product::whereIn('id', [1, 2])->get();
        $products = Product::where('name', 'LIKE', '%' . '2' . '%')->get();
        $products = Product::whereBetween('id', [1, 2])->get();
        $products = Product::whereOr('id', 1)->whereOr('id', 2)->get();

        $products = Product::paginate(2);


        return view('Product.index')->with(['currentUser' => $currentUser, 'products' => $products, 'title' => 'Listado de Productos del sistema', 'isAuth' => $isAuth]);
    }

    public function update()
    {

        return view('Product.index')->with(['products' => $products, 'title' => 'Listado de Productos del sistema']);
    }
}
