<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index($titulo = '')
    {
        $users = User::all();

        if (empty($titulo)){
            $titulo = 'Listado de Usuarios del sistema';
        }

        return view('User.index')->with(['users' => $users, 'title' => $titulo]);
    }
}
